package com.example.epsi.ui.garage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.epsi.core.ViewModelFactory
import com.example.epsi.databinding.ActivityGarageDetailBinding

class GarageDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGarageDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGarageDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val factory: ViewModelProvider.Factory = ViewModelFactory(application)
        val garageViewModel = ViewModelProvider(this, factory).get(GarageViewModel::class.java)
    }
}
